"use strict";
const common_vendor = require("../../common/vendor.js");
const PageBase = () => "../../components/PageBase.js";
const FindToFind = () => "../../components/FindToFind.js";
const _sfc_main = {
  data() {
    return {
      menuList: [
        {
          color: "#fce2e1",
          title: "找达人",
          image: "../../static/达人.png",
          desc: "选择多"
        },
        {
          color: "#fdf4d7",
          title: "找合作",
          image: "../../static/合作.png",
          desc: "省时间"
        },
        {
          color: "#dcf5ef",
          title: "找社群",
          image: "../../static/社群.png",
          desc: "交朋友"
        }
      ],
      findMenuActiveID: 1001,
      findToMenuLetter: [
        { id: 1001, name: "抖音号" },
        { id: 1002, name: "视频号" },
        { id: 1003, name: "小红书" }
      ]
    };
  },
  components: {
    FindToFind,
    PageBase
  },
  onLoad() {
  }
};
if (!Array) {
  const _component_find_to_find = common_vendor.resolveComponent("find-to-find");
  const _component_PageBase = common_vendor.resolveComponent("PageBase");
  (_component_find_to_find + _component_PageBase)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.menuList, (item, index, i0) => {
      return {
        a: item.image,
        b: common_vendor.s("background-color:" + item.color),
        c: common_vendor.t(item.title),
        d: common_vendor.t(item.desc),
        e: index
      };
    }),
    b: common_vendor.f($data.findToMenuLetter, (item, index, i0) => {
      return {
        a: common_vendor.t(item.name),
        b: common_vendor.n($data.findMenuActiveID == item.id ? "active" : ""),
        c: item.id
      };
    }),
    c: common_vendor.p({
      title: "新榜新桥",
      isTabbarPage: true,
      styles: ["background:linear-gradient(to right, #fdc787, #fae2c0);"]
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/bridge/bridge.vue"]]);
wx.createPage(MiniProgramPage);
