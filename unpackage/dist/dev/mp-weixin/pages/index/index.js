"use strict";
const common_vendor = require("../../common/vendor.js");
const BaseTitleWrapper = () => "../../components/BaseTitleWrapper.js";
const Cooperation = () => "../../components/Cooperation.js";
const SwiperCustom = () => "../../components/SwiperCustom.js";
const _sfc_main = {
  data() {
    return {
      menuList: [
        { menuName: "微信", id: 1001 },
        { menuName: "视频号", id: 1002 },
        { menuName: "微博", id: 1003 },
        { menuName: "抖音号", id: 1004 },
        { menuName: "快手号", id: 1005 },
        { menuName: "小红书", id: 1006 },
        { menuName: "哔哩哔哩", id: 1007 },
        { menuName: "查看全部", id: 1008 }
      ],
      cooperations: [
        {
          title: "找达人",
          desc: "速度快",
          id: 2001
        },
        {
          title: "找合作",
          desc: "选择多",
          id: 2002
        },
        {
          title: "找社群",
          desc: "交朋友",
          id: 2003
        }
      ]
    };
  },
  components: {
    SwiperCustom,
    BaseTitleWrapper,
    Cooperation
  },
  onLoad() {
  },
  methods: {}
};
if (!Array) {
  const _component_SwiperCustom = common_vendor.resolveComponent("SwiperCustom");
  const _component_BaseTitleWrapper = common_vendor.resolveComponent("BaseTitleWrapper");
  const _component_Cooperation = common_vendor.resolveComponent("Cooperation");
  (_component_SwiperCustom + _component_BaseTitleWrapper + _component_Cooperation)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.menuList, (item, index, i0) => {
      return {
        a: common_vendor.t(item.menuName),
        b: item.id
      };
    }),
    b: common_vendor.p({
      title: "新榜榜单"
    }),
    c: common_vendor.f($data.cooperations, (item, index, i0) => {
      return {
        a: item.id,
        b: "70ffaa46-3-" + i0 + ",70ffaa46-2",
        c: common_vendor.p({
          cooperation: item
        })
      };
    }),
    d: common_vendor.p({
      title: "商业合作"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
