"use strict";
const common_vendor = require("../../common/vendor.js");
const PageBase = () => "../../components/PageBase.js";
const _sfc_main = {
  data() {
    return {};
  },
  components: {
    PageBase
  }
};
if (!Array) {
  const _component_PageBase = common_vendor.resolveComponent("PageBase");
  _component_PageBase();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      title: "个人中心",
      isTabbarPage: true,
      styles: ["background:linear-gradient(to right, #fdc685, #fce7bc);"]
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/my/my.vue"]]);
wx.createPage(MiniProgramPage);
