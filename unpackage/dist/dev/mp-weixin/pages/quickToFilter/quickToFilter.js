"use strict";
const common_vendor = require("../../common/vendor.js");
const PageBase = () => "../../components/PageBase.js";
const _sfc_main = {
  data() {
    return {};
  },
  components: {
    PageBase
  }
};
if (!Array) {
  const _component_PageBase = common_vendor.resolveComponent("PageBase");
  _component_PageBase();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f(6, (item, k0, i0) => {
      return {};
    }),
    b: common_vendor.p({
      isTabbarPage: false,
      title: "快速筛选达人",
      styles: ["background:linear-gradient(to right, #fcc17d, #f9e4c5);"]
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/quickToFilter/quickToFilter.vue"]]);
wx.createPage(MiniProgramPage);
