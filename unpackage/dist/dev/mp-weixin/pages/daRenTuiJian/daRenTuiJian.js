"use strict";
const common_vendor = require("../../common/vendor.js");
const PageBase = () => "../../components/PageBase.js";
const FindToFind = () => "../../components/FindToFind.js";
const _sfc_main = {
  data() {
    return {
      menuList: [
        {
          id: 30001,
          title: "达人广场"
        },
        {
          id: 30002,
          title: "达人资源包"
        },
        {
          id: 30003,
          title: "最新达人"
        },
        {
          id: 30004,
          title: "批量联系"
        }
      ],
      findMenuActiveID: 1001,
      findToMenuLetter: [
        {
          id: 1001,
          name: "抖音号"
        },
        {
          id: 1002,
          name: "视频号"
        },
        {
          id: 1003,
          name: "小红书"
        }
      ]
    };
  },
  components: {
    PageBase,
    FindToFind
  }
};
if (!Array) {
  const _component_find_to_find = common_vendor.resolveComponent("find-to-find");
  const _component_PageBase = common_vendor.resolveComponent("PageBase");
  (_component_find_to_find + _component_PageBase)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.menuList, (item, index, i0) => {
      return {
        a: common_vendor.t(item.title),
        b: item.id
      };
    }),
    b: common_vendor.f($data.findToMenuLetter, (item, index, i0) => {
      return {
        a: common_vendor.t(item.name),
        b: common_vendor.n($data.findMenuActiveID == item.id ? "active" : ""),
        c: item.id
      };
    }),
    c: common_vendor.p({
      isTabbarPage: false,
      title: "达人推荐",
      styles: ["background:#f8f8f8;"]
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/daRenTuiJian/daRenTuiJian.vue"]]);
wx.createPage(MiniProgramPage);
