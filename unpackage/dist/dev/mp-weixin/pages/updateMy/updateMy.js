"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_apis_userModalApi = require("../../utils/apis/userModalApi.js");
require("../../utils/http.js");
const PageBase = () => "../../components/PageBase.js";
const _sfc_main = {
  data() {
    return {};
  },
  created() {
  },
  methods: {
    //获取手机号
    getUserInfo(userInfo) {
      console.log("获取用户手机号", userInfo);
    },
    //用户登录
    login() {
      this.getUnionid().then((res) => {
        utils_apis_userModalApi.userModalApi.login({ uniqueId: res.unionid }).then((login) => {
          console.log("登录-----", login);
        });
      });
    },
    getUnionid() {
      return new Promise((resolve, reject) => {
        common_vendor.index.login({
          provider: "weixin",
          success(res) {
            utils_apis_userModalApi.userModalApi.getUnionid({ code: res.code }).then((result2) => {
              resolve(result2.data);
            });
          }
        });
      });
    },
    //用户注册
    regiser() {
      this.getUnionid().then((res) => {
        utils_apis_userModalApi.userModalApi.register({
          username: "陈凯飞",
          phone: "18219015944",
          uniqueId: result.unionid,
          openId: result.openId
        }).then((register) => {
          console.log("用户注册结果", register);
        });
      });
    }
  },
  components: {
    PageBase
  }
};
if (!Array) {
  const _component_PageBase = common_vendor.resolveComponent("PageBase");
  _component_PageBase();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.login && $options.login(...args)),
    b: common_vendor.o((...args) => $options.regiser && $options.regiser(...args)),
    c: common_vendor.p({
      title: "编辑个人资料",
      isTabbarPage: false,
      styles: ["background:linear-gradient(to right, #fdc685, #fce7bc);"]
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/pages/updateMy/updateMy.vue"]]);
wx.createPage(MiniProgramPage);
