"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  name: "NavCuston",
  props: {
    title: {
      type: String,
      default: ""
    },
    styles: {
      type: Array,
      default: () => []
    },
    isTabbarPage: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      statusBarHeight: 0,
      statusBarTop: 0,
      menuButtonInfo: {
        width: 0,
        height: 0,
        top: 0,
        right: 0
      }
    };
  },
  methods: {
    //导航栏回退
    toNavBack() {
      common_vendor.index.navigateBack(-1);
    },
    renderToNav() {
      const that = this;
      common_vendor.index.getSystemInfo({
        success(res) {
          that.statusBarHeight = res.statusBarHeight * 2;
          that.statusBarTop = res.safeArea.top;
          that.menuButtonInfo = common_vendor.index.getMenuButtonBoundingClientRect();
          that.$emit("navFinish", { height: res.statusBarHeight * 2 + res.safeArea.top });
        }
      });
    }
  },
  mounted() {
    this.renderToNav();
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.t($props.title),
    b: !$props.isTabbarPage
  }, !$props.isTabbarPage ? {
    c: common_vendor.o((...args) => $options.toNavBack && $options.toNavBack(...args)),
    d: common_vendor.s("height:" + ($data.menuButtonInfo.height - 12) + "px"),
    e: common_vendor.s("height:" + ($data.menuButtonInfo.height - 12) + "px"),
    f: common_vendor.s("height:" + ($data.menuButtonInfo.height - 12) + "px"),
    g: common_vendor.s("height:" + $data.menuButtonInfo.height + "px;width:" + $data.menuButtonInfo.width + "px;top:50%;border-radius:" + $data.menuButtonInfo.height + "px;right:" + ($data.menuButtonInfo.right - $data.menuButtonInfo.width) + "px;")
  } : {}, {
    h: common_vendor.s("height:" + $data.statusBarHeight + "px;top:" + $data.statusBarTop + "px;line-height:" + $data.statusBarHeight + "px;"),
    i: common_vendor.s("height:" + ($data.statusBarHeight + $data.statusBarTop) + "px;"),
    j: common_vendor.s($props.styles)
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/NavCustom.vue"]]);
wx.createComponent(Component);
