"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  props: {
    title: {
      type: String,
      default: ""
    }
  },
  name: "BaseTitleWrapper",
  data() {
    return {};
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($props.title)
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/BaseTitleWrapper.vue"]]);
wx.createComponent(Component);
