"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  name: "Cooperation",
  props: {
    cooperation: {
      type: Object,
      default: () => {
      }
    }
  },
  data() {
    return {};
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($props.cooperation.title),
    b: common_vendor.t($props.cooperation.desc)
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/Cooperation.vue"]]);
wx.createComponent(Component);
