"use strict";
const common_vendor = require("../common/vendor.js");
const NavCustom = () => "./NavCustom.js";
const _sfc_main = {
  name: "PageBase",
  props: {
    title: {
      type: String,
      default: ""
    },
    styles: {
      type: Array,
      default: () => []
    },
    isTabbarPage: {
      type: Boolean,
      default: false
    }
  },
  components: {
    NavCustom
  },
  methods: {
    //导航栏初始化完成
    navFinish(e) {
      this.navHeight = e.height;
    }
  },
  data() {
    return {
      navHeight: 0
    };
  }
};
if (!Array) {
  const _component_NavCustom = common_vendor.resolveComponent("NavCustom");
  _component_NavCustom();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o($options.navFinish),
    b: common_vendor.p({
      styles: $props.styles,
      isTabbarPage: $props.isTabbarPage,
      title: $props.title
    }),
    c: common_vendor.s("height:calc(100% - " + $data.navHeight + "px)")
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/PageBase.vue"]]);
wx.createComponent(Component);
