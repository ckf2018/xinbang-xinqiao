"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  name: "FindToFind",
  data() {
    return {};
  },
  methods: {
    toMoreByType() {
      common_vendor.index.navigateTo({
        url: "/pages/daRenTuiJian/daRenTuiJian"
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.toMoreByType && $options.toMoreByType(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/FindToFind.vue"]]);
wx.createComponent(Component);
