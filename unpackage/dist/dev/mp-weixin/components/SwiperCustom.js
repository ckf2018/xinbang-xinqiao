"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  name: "SwiperCustom",
  data() {
    return {
      indicatorDots: false,
      autoplay: true,
      interval: 2e3,
      duration: 500,
      indicatorDotActiveIndex: 0,
      lists: [
        {
          images: "https://img30.360buyimg.com/pop/s1180x940_jfs/t1/164800/35/14493/97067/64f5b323F8d09f2ba/385018cf1f0e00b5.jpg.avif",
          title: ""
        },
        {
          images: "https://img30.360buyimg.com/pop/s1180x940_jfs/t1/164800/35/14493/97067/64f5b323F8d09f2ba/385018cf1f0e00b5.jpg.avif",
          title: ""
        },
        {
          images: "https://img30.360buyimg.com/pop/s1180x940_jfs/t1/164800/35/14493/97067/64f5b323F8d09f2ba/385018cf1f0e00b5.jpg.avif",
          title: ""
        }
      ]
    };
  },
  methods: {
    swiperChangeHandle(e) {
      this.indicatorDotActiveIndex = e.detail.current;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.lists, (item, index, i0) => {
      return {
        a: index,
        b: item.images
      };
    }),
    b: common_vendor.o((...args) => $options.swiperChangeHandle && $options.swiperChangeHandle(...args)),
    c: $data.indicatorDots,
    d: $data.autoplay,
    e: $data.interval,
    f: $data.duration,
    g: common_vendor.f($data.lists, (dot, index, i0) => {
      return {
        a: "d_" + index,
        b: common_vendor.n($data.indicatorDotActiveIndex === index ? "indicator_active" : "")
      };
    })
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/kaifeichen/Desktop/projects/xinbang-xinqiao/components/SwiperCustom.vue"]]);
wx.createComponent(Component);
