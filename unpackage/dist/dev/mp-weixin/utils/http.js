"use strict";
const common_vendor = require("../common/vendor.js");
const http = {
  baseUrl: "https://api.xiaolingyouxuan.com/doudian",
  //发送请求方法
  request(config) {
    config = beforeRequest(config);
    config.url = this.baseUrl + config.url;
    return new Promise((resolve, reject) => {
      common_vendor.index.request(config).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  get(url, params) {
    return this.request({
      url,
      data: params,
      method: "GET"
    });
  },
  post(url, params) {
    return this.request({
      url,
      data: params,
      method: "POST"
    });
  },
  delete(url, params) {
    return this.request({
      url,
      data: params,
      method: "DELETE"
    });
  }
};
const beforeRequest = (config) => {
  return config;
};
exports.http = http;
