"use strict";
const utils_http = require("../http.js");
const userModalApi = {
  //用户登录
  login(data) {
    return new Promise((resolve, reject) => {
      utils_http.http.post("/user/login", data).then((res) => {
        resolve(res.data);
      });
    });
  },
  //用户注册
  register(data) {
    return new Promise((resolve, reject) => {
      utils_http.http.post("/user/register", data).then((res) => {
        resolve(res.data);
      });
    });
  },
  getUnionid(data) {
    return new Promise((resolve, reject) => {
      utils_http.http.get("/wechat/getUnionid", data).then((res) => {
        resolve(res.data);
      });
    });
  }
};
exports.userModalApi = userModalApi;
