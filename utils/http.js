const http = {
	baseUrl: 'https://api.xiaolingyouxuan.com/doudian',
	//发送请求方法
	request(config) {
		config = beforeRequest(config)
		config.url = this.baseUrl + config.url
		//创建一个promise对象
		return new Promise((resolve, reject) => {
			uni.request(config)
				.then(res => {
					resolve(res)
				})
				.catch(err => {
					reject(err)
				})
		})
	},
 
	get(url, params) {
		// url: 接口地址
		// data: 查询参数
		// auth：请求是否需要携带token进行认证（true/false）
		return this.request({
			url: url,
			data: params,
			method: 'GET'
		})
	},
	post(url, params) {
		return this.request({
			url: url,
			data: params,
			method: 'POST'
		})
	},
	delete(url, params) {
		return this.request({
			url: url,
			data: params,
			method: 'DELETE'
		})
	},
};
//请求拦截器
const beforeRequest = (config) => {
	//请求之前要做的操作
	return config
}
//响应拦截器
const beforeResponse = (response) => {
	//响应之后要做的操作
	return response
}
//异常处理器
const errorHandle = (err) => {
	//响应之后要做的操作
}
 
export default http