import http  from "../http.js";
export default {
	//用户登录
	login(data){
		return new Promise((resolve,reject)=>{
			http.post("/user/login",data).then((res)=>{
				resolve(res.data)
			})
		})
	},
	//用户注册
	register(data){
		return new Promise((resolve,reject)=>{
			http.post("/user/register",data).then((res)=>{
				resolve(res.data)
			})
		})
	},
	getUnionid(data){
		return new Promise((resolve,reject)=>{
			http.get("/wechat/getUnionid",data).then((res)=>{
				resolve(res.data)
			})
		})
	}
	
}